Contact message feedback form anonymous
================
A simple module that enables anonymous users to "send yourself message" on
Drupal's core contact module.

Background
================
By default the option to send yourself a message in Drupal is disabled by
default. e.g. in web/core/modules/contact/src/MessageForm.php we have:

```
$form['copy'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Send yourself a copy'),
      // Do not allow anonymous users to send themselves a copy, because it can
      // be abused to spam people.
      '#access' => $user->isAuthenticated(),
    ];
```

So we can see that this feature is disabled by default with no way to enable it.

Installation
------------
Download and install like any other Drupal module. Once enabled, anonymous users
should be able to access the option to send them selves a copy of any message
sent using the contact form.
